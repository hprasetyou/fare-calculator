<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Route;

class CalculatorController extends Controller
{

    public function getFare(Request $request){
        $fares = Route::getFare($request->query('origin'),$request->query('dest'),$request->query('weight'));
        if($fares){
            return $fares->toJson(JSON_PRETTY_PRINT);
        }else{
            return response()
            ->json(['error' => 'not found'], 200);
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\LocationRoute;

class LocationController extends ResourceController
{
    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $inRoute = LocationRoute::where('location_id',$id);
        $location = $this->model::find($id);
        if($inRoute->count() > 0){
            $route = $inRoute->first()->route;
            return response()
            ->json(['error'=>'No Exception','message'=>'Cannot delete ' . $location->name . '. It is used on ' . $route->name . (($inRoute->count() > 1)?' and ' . ($inRoute->count() - 1) . ' other':'') . ' route' ]);
        }else{
            return parent::destroy($id);
        }
    }

    protected function save(Request $request, int $id = null){
        $data = $this->prepareStoreData($request, (int)$id);
        $exist = $this->model::where('name',$data->name);
        if($exist->count() > 0){
            return $exist->first();
        }else{
            $data->save();
            $this->afterStoreData($request, $data);
            return $data;
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Location;
use App\Fare;
use App\LocationRoute;

class RouteController extends ResourceController
{    

    protected $with = ['locations'];
    protected function prepareStoreData(Request $request, int $id = null){
        $reqData = json_decode($request->getContent(), true);
        if(!$id){
            $data = new $this->model;
        }else{
            $data = $this->model::findOrFail($id);
        }
        $dOrigin = $this->flattenLoc($reqData['origin']);
        $dDestination = $this->flattenLoc($reqData['destination']);
        
        if(!isset($reqData['name'])){
            $routeName = '';
            $origins = Location::whereIn('id',$dOrigin)->get();
            $destination = Location::whereIn('id',$dDestination)->get();
            foreach ($origins as $key => $value) {
                $routeName .= $value->name . ' ';
            }
            $routeName .= ' -> ';
            foreach ($destination as $key => $value) {
                $routeName .= $value->name . ' ';
            }
            $data->name = $routeName;
        }else{
            $data->name = $reqData['name'];
        }

        $data->origin = serialize($dOrigin);
        $data->destination = serialize($dDestination);
        return $data;
    }
    protected function afterStoreData(Request $request,$data){
        parent::afterStoreData($request,$data);

        $origin = unserialize($data->origin);
        $destination = unserialize($data->destination);
        foreach ($origin as $originId) {
            LocationRoute::firstOrCreate(['route_id'=>$data->id,'location_id'=>$originId,'type'=>'origin']);
        }
        foreach ($destination as $destId) {
            LocationRoute::firstOrCreate(['route_id'=>$data->id,'location_id'=>$destId,'type'=>'destination']);
        }
        LocationRoute::where(['route_id'=>$data->id])->whereNotIn('location_id',array_merge($origin,$destination))->delete();

        $reqData = json_decode($request->getContent(), true);
        if(isset($reqData['fares'])){
            foreach ($reqData['fares'] as $fare) {
                if(isset($fare['id'])){
                    $mFare = Fare::find($fare['id']);
                }else{
                    $mFare = new Fare;
                }
                $mFare->route_id = $data->id;
                $mFare->base_fare = $fare['base_fare'];
                $mFare->fpk = $fare['fpk'];
                $mFare->min_kg = $fare['min_kg'];
                $mFare->offset = $fare['offset'];
                $mFare->save();
            }
        }
    }

    private function flattenLoc($loc){
        return array_map(function($item){return isset($item['value'])?$item['value']:$item;},$loc);
    }

    protected function prepareShowData(Request $request, $id){
        $data = $this->model::with('fares')->find($id);
        $locations = LocationRoute::where('route_id',$id)->get();
        $origin = [];
        $destination = []; 
        foreach ($locations as $location) {
            if($location->type == "origin"){
                array_push($origin,['value'=>$location->location->id,'text'=>$location->location->name]);
            }else{
                array_push($destination,['value'=>$location->location->id,'text'=>$location->location->name]);
            }
        }
        $data->origin = $origin;
        $data->destination = $destination;
        return $data;
    }
        /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $inRoute = LocationRoute::where('route_id',$id)->delete();
        parent::destroy($id);
    }

   
}

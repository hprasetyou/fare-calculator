<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\LocationRoute;
use App\Fare;

class Route extends Model
{
    //
    public function fares()
    {
        return $this->hasMany('App\Fare');
    }
    public function locations()
    {
        return $this->belongsToMany('App\Location');
    }

    static function getFare($src,$dest,$weight)
    {
        $filterSrc = LocationRoute::where('type','origin')->where('location_id',$src)->get();
        $filterDest = LocationRoute::where('type','destination')->where('location_id',$dest)->get();
        $route_id = false;
        foreach ($filterSrc as $src) {
            foreach ($filterDest as $dst) {
               if($src->route_id == $dst->route_id){
                    $route_id = $src->route_id;
               }
            }
        }
        if($route_id){
            $fare = Fare::where('route_id',$route_id)->where('min_kg','<',$weight)->orderBy('min_kg', 'desc')->firstOrFail();
            if($fare){
                $fare->routeInfo = self::find($route_id);
            }
            return $fare;
        }else{
            return false;
        }
    }

}

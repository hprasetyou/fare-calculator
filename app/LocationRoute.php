<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationRoute extends Model
{
    
    protected $table = 'location_route';
    protected $fillable = ['route_id','location_id','type'];

    public function location()
    {
        return $this->belongsTo('App\Location');
    }
    public function route()
    {
        return $this->belongsTo('App\Route');
    }
}

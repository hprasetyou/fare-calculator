<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Using Closure based composers...
        $menu = [
            ['name'=>'Dashboard',
            'url'=>'/',
            'icon'=>'home'],
            ['name'=>'Pricing',
            'icon'=>'book',
            'menus'=> [
                ['name'=>'Locations',
                'url'=>'/locations',
                'icon'=>'account_circle'],
                ['name'=>'Routes',
                'url'=>'/routes',
                'icon'=>'account_circle']
                ]
            ]
        ];
        View::share('foo', json_encode($menu));
    }
}

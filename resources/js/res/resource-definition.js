import users from './resources/users';
import routes from './resources/routes';
import locations from './resources/locations';


export default {
    users,
    routes,
    locations
}
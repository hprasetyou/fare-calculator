export default {
    title:'Routes',
    header:[{
        text: 'ID',
        align: 'left',
        value: 'id'
    },
    {
        text: 'Name',
        value: 'name'
    }],
    filterable:[{
        name:'name',
        label:'Name',
        type:'text'
    }],
    formDefinition:{
        left:[
            {
                label:'Name',
                model:'name',
                type:'input'
            },
            {
                label:'Origin',
                model:'origin',
                type:'one2many',
                objUrl:'location?filter[]=is_origin&filterValue[]=1&filterOperator==',
                disableAdd:true,
                multiple:true
            },
            {
                label:'Destination',
                model:'destination',
                type:'one2many',
                objUrl:'location',
                disableAdd:true,
                multiple:true
            },
        ],
        child:[
            {
                label:'Fares',
                model:'fares',
                header:[{
                    label: 'Base Fare',
                    model: 'base_fare',
                    type: 'input',
                    editable: true,
                    default:1
                },{
                    label: 'Bounds',
                    model: 'min_kg',
                    type: 'input',
                    editable: true,
                    default:1
                },{
                    label: 'Fare per kg',
                    model: 'fpk',
                    type: 'input',
                    editable: true,
                    default:1
                },{
                    label: 'Offset',
                    model: 'offset',
                    type: 'input',
                    editable: true,
                    default:1
                }],
            }
        ]
    },
    actions:[{
        name:'edit',
        icon:'edit'
    }],
    dataUrl:'/api/route'
}